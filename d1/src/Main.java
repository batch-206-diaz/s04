public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        Car car1 = new Car();
        //System.out.println(car1.brand);
        //System.out.println(car1.make);
        //System.out.println(car1.price);

        //car1.make = "Veyron";
        //car1.brand = "Bugatti";
        //car1.price = 2000000;
        //System.out.println(car1.make);
        //System.out.println(car1.brand);
        //System.out.println(car1.price);

        Car car2 = new Car();
        //car2.make = "Tamaraw FX";
        //car2.brand = "Toyota";
        //car2.price = 450000;
        //System.out.println(car2.make);
        //System.out.println(car2.brand);
        //System.out.println(car2.price);

        Car car3 = new Car();
        //car3.make = "Model S";
        //car3.brand = "Tesla";
        //car3.price = 10990;
        //System.out.println(car3.make);
        //System.out.println(car3.brand);
        //System.out.println(car3.price);

        Car car4 = new Car();
        //System.out.println(car4.brand);

        Driver driver1 = new Driver("Alejandro",25);
        Car car5 = new Car("Vios","Toyota",1500000,driver1);
        //System.out.println(car5.make);
        //System.out.println(car5.brand);
        //System.out.println(car5.price);

        car1.start();
        car2.start();
        car3.start();
        car4.start();
        car5.start();

        System.out.println(car1.getMake());
        System.out.println(car5.getMake());

        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car5.setMake("Innova");
        //System.out.println(car5.make);

        car1.setBrand("Bugatti");
        car1.setPrice(2000000);
        System.out.println(car1.getMake());
        System.out.println(car1.getBrand());
        System.out.println(car1.getPrice());

        System.out.println(car5.getCarDriver()); //memory address for object
        System.out.println(car5.getCarDriver().getName()); //gets the driver name

        Driver newDriver = new Driver("Antonio",21); //new driver
        car5.setCarDriver(newDriver); //can set new driver
        System.out.println(car5.getCarDriver().getName()); //show new driver
        System.out.println(car5.getCarDriver().getAge()); //show age

        System.out.println(car5.getCarDriverName()); //used custom method


        // Activity

        Animal animal1 = new Animal("Chubby","brown");
        animal1.call();

    }
}
