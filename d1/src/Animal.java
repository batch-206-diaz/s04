public class Animal {
    private String name;
    private String color;

    //constructors
    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public Animal() {
    }

    //getters
    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    //setters
    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    //method
    public void call(){
        System.out.println("Hi! My name is " + getName() + ".");
    }
}

