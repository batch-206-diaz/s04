public class Car {

    //public String make;
    //public String brand;
    //public int price;

    private String make;
    private String brand;
    private int price;

    private Driver carDriver;

    //default constructor
    public Car(){
        this.make = "Random";
        this.brand = "Geely";
        this.carDriver = new Driver();
    }

    //parameterized constructor
    public Car(String make, String brand, int price, Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    //method
    public void start(){
        System.out.println("Vrooom! Vrooom!");
    }

    //custom method
    public String getCarDriverName(){
        return this.carDriver.getName();
    }

    //getters
    public String getMake(){
        return this.make;
    }
    public String getBrand(){
        return this.brand;
    }

    public int getPrice(){
        return this.price;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    //setters
    public void setMake(String makeParams){
        this.make = makeParams;
    }

    public void setBrand(String brandParams){
        this.brand = brandParams;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }



}

/*
    Properties/attributes
        - the characteristics of the object the class will create.
        - Read-only - will only have getters and no setters

    Constructor
        - method to create the object and instantiate with its initialized values.
        - java assigns a value if there is no constructor
        - empty/default - allows to set the initial values of an instance
        - parameterized - allows to set with the use of parameters

    Methods
        - are functions of an object/instance which perform certain tasks
        - void > function does not return anything
        - a method's return dataType must also be declared

    Encapsulation
        - the bundling of data, along with methods that operate on that data, into a single unit
        - Data Hiding
            - variables of a class will be hidden from other classes and can only be accessed only through getters and setters instead of just dot notation
        - Encapsulation is achieved by declaring variables of classes as private and by adding getters and setters
            - public - variable/property accessible anywhere in the application
            - private - limits the access and ability to get or set only within its own class
            - getters - methods that return the value of the given property
            - setters - methods that allow to set the value of the property

    Inheritance
        - Composition
            - allows modelling objects to be made up of other objects. Classes can have instances of other classes.
*/